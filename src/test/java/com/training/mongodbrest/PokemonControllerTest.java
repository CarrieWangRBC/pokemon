package com.training.mongodbrest;

import com.training.mongodbrest.controller.PokemonController;
import com.training.mongodbrest.model.Pokemon;
import com.training.mongodbrest.service.PokemonServiceInterface;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PokemonController.class)
public class PokemonControllerTest {

    @Autowired
    private MockMvc mockMvc; //instead of making a controller class, ask spring to make a controller class and
    //add a web layer with it, so the test can actually send HTTP msg to the layer

    @MockBean
    private PokemonServiceInterface pokemonService;

    @Test
    public void testPokemonControllerFindAll() throws Exception {
        List<Pokemon> allPokemon = new ArrayList<Pokemon>();
        Pokemon testPokemon = new Pokemon();
        testPokemon.setId("adbcd");
        allPokemon.add(testPokemon);

        when(pokemonService.findAll()).thenReturn(allPokemon);

        this.mockMvc.perform(get("/api/v1/pokemon"))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
