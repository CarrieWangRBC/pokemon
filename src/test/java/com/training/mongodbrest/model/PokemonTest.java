package com.training.mongodbrest.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.Assertions.assertEquals;



public class PokemonTest {


    @BeforeEach
    public void runBeforeEachTest(){
        System.out.println("test start");
    }

    @Test
    public void testPokemonGetterSetters(){
        Pokemon testPokemon = new Pokemon();
        testPokemon.setId("1234");
        testPokemon.setName("Pikachu");
        testPokemon.setType("Electric");

//        assert(testPokemon.getId().equals("1234"));
//        assert (testPokemon.getName().equals("Pikachu"));

        assert(testPokemon.getId().equals("1234"));
        assert (testPokemon.getName().equals("Pikachu"));
    }
}
