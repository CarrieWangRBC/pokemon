package com.training.mongodbrest.controller;

import com.training.mongodbrest.model.Pokemon;
import com.training.mongodbrest.service.PokemonService;
import com.training.mongodbrest.service.PokemonServiceInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController // 1. makes it a component 2. library web related
@RequestMapping("/api/v1/pokemon")  //at different path now, its at http://localhost:8080/api/v1/pokemon
//we can have different controller for different path

public class PokemonController {

    //for logging
    private static final Logger LOG = LoggerFactory.getLogger(PokemonController.class);
    //creates an internal tree of objects in the class for logging
    //level (for up to down): critical error warm info debug
    //we can set log level for each object , for eg set mongodbtest log level be warning, anything below warning wont be printed


//    @Bean and @Autowired do two very different things. The other answers here explain in a little more detail, but at a simpler level:
//
//    @Bean tells Spring 'here is an instance of this class, please keep hold of it and give it back to me when I ask'.
//
//    @Autowired says 'please give me an instance of this class, for example, one that I created with an @Bean annotation earlier'.

    //spring would create one so we dont need to Service service = new service(), it needs to create new repo.
    @Autowired
    private PokemonServiceInterface pokemonService;

//    @GetMapping("findAll")  //at different path now, its at http://localhost:8080/api/v1/pokemon/findAll  but usually we dont do it
    //because findall should be implied in GET, we are doing resource oriented not action oriented
    @GetMapping
    public List<Pokemon> findAll(){
        //GET /api/v1/pokemon will get all pokemon
        return pokemonService.findAll();
    }

//    @GetMapping("{id}")
//    public Pokemon findById(@PathVariable String id){
//        //GET /api/v1/pokemon/{id} will get the corresponding pokemon
//        return pokemonService.findById(id).get();
//    }

    //below is similar function to above
//
    @GetMapping("{id}")
    public ResponseEntity<Pokemon> findById(@PathVariable String id){
        LOG.debug("findById is being called.");
        try {
            return new ResponseEntity<Pokemon>(pokemonService.findById(id).get(), HttpStatus.OK);
        }
        catch (NoSuchElementException ex){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public Pokemon save(@RequestBody Pokemon pokemon){ //reqeustbody convert whatever input to pokemmon type
        return pokemonService.save(pokemon);
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id){
        //DELETE /api/v1/pokemon/{id}
        if(pokemonService.findById(id).isEmpty()){
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        } //do you rlly want to do 2 data query? it's not necessary to do a empty check here
        pokemonService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);



    }
}
