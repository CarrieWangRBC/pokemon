package com.training.mongodbrest.service;

import com.training.mongodbrest.model.Pokemon;
import com.training.mongodbrest.repository.PokemonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Component
public class PokemonService implements PokemonServiceInterface { //good practice to use internface

    @Autowired
    private PokemonRepository pokemonRepository;

    public Optional<Pokemon> findById(String id){
        return pokemonRepository.findById(id);
    }

    public List<Pokemon> findAll(){
        return pokemonRepository.findAll();
    }

    public Pokemon save(Pokemon pokemon) {

        return pokemonRepository.save(pokemon);
    }

    public void delete(String id) {
        pokemonRepository.deleteById(id);
    }
}
