package com.training.mongodbrest.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter  //from lombok depedency, we dont need extra getter or setter
@Document //mongo now knows this can be saved to mongo
//@Document(collection="pokemon_collection") this will specify which collection to use
public class Pokemon {

    @Id
    private String id;
    private String name;
    private String type;

//    public String getId() {
//        return id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
}
