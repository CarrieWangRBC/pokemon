package com.training.mongodbrest.repository;

import com.training.mongodbrest.model.Pokemon;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface PokemonRepository extends MongoRepository<Pokemon,String> {
    //mongorepository is an interface

    //defaulting to test in mongodb

    //to go to a different database name or. go to application.property
    //to specify collection, check in model-> Pokemon
}
